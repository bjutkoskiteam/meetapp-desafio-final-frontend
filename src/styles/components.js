import styled from 'styled-components';

export const DefaultContainer = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const SignForm = styled.form`
  width: 100%;

  display: flex;
  flex-direction: column;
  align-items: stretch;
`;

export const Button = styled.button`
  color: inherit;
  font-weight: bold;
  background: #e5556e;
  height: 44px;
  border-radius: 50px;
  line-height: 32px;
  border: 0;
  margin-bottom: 25px;
  font-size: 16px;

  &:hover {
    background: #e5456e;
  }
`;

export const Field = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  margin-bottom: 30px;
  width: 100%;

  span {
    font-size: 16px;
    font-weight: bold;
  }

  input[type='text'],
  input[type='email'],
  input[type='password'] {
    background: inherit;
    border: 0;
    height: 44px;
    font-size: 20px;
    color: #fff;

    &::placeholder {
      color: #b3b3b3;
    }
  }
`;
