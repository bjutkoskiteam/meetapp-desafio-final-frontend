import React, { Component, Fragment } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Creators as SessionActions } from '../../store/ducks/session';

class Session extends Component {
  componentDidMount() {
    const { verifySessionRequest } = this.props;
    verifySessionRequest();
  }

  render() {
    const { children, session } = this.props;
    return <Fragment>{!session.loading && children}</Fragment>;
  }
}

Session.propTypes = {
  verifySessionRequest: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  session: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
  }).isRequired,
};

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => bindActionCreators(SessionActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Session);
