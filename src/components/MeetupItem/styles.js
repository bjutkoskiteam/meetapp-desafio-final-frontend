import styled from 'styled-components';

export const Container = styled.div`
  height: 200px;
  width: 290px;
  background-color: #fff;
  border-radius: 5px;
  overflow: hidden;
  margin-right: 20px;
  min-width: 290px;
  cursor: pointer;

  a {
    display: flex;
    flex-direction: column;
    text-decoration: none;
    height: 100%;
    width: 100%;

    img {
      height: 60%;
      width: 100%;
    }

    div {
      display: flex;
      flex-direction: row;
      height: 40%;
    }
  }

  &:hover {
    opacity: 0.4;
  }
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column !important;
  align-items: flex-start;
  padding: 15px;
  height: 100% !important;
  width: 70%;

  h1 {
    font-weight: bold;
    font-size: 16px;
    color: #222222;
    margin-bottom: 5px;
  }

  p {
    font-size: 14px;
    color: #999999;
  }
`;
export const Image = styled.div`
  height: 100% !important;
  width: 30%;
  margin: auto;

  div {
    margin: auto;
    width: 40px !important;
    height: 40px !important;
    background: #e5556e;
    border-radius: 50%;

    img {
      margin: auto;
      width: 24px;
      height: 24px;
    }
  }
`;
