import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Container, Info, Image } from './styles';

import ButtonImage from '../../assets/images/chevron_right.png';

const MeetupItem = ({ meetup }) => (
  <Container>
    <Link to={`/meetups/${meetup.id}`}>
      <img src={meetup.file.url} alt="Meetup" />
      <div>
        <Info>
          <h1>{meetup.title}</h1>
          <p>{`${meetup.subscriptions.length} membros`}</p>
        </Info>
        <Image>
          <div>
            <img src={ButtonImage} alt="Botao" />
          </div>
        </Image>
      </div>
    </Link>
  </Container>
);

MeetupItem.propTypes = {
  meetup: PropTypes.shape({
    id: PropTypes.number.isRequired,
    file: PropTypes.shape({
      url: PropTypes.string.isRequired,
    }).isRequired,
    title: PropTypes.string.isRequired,
    subscriptions: PropTypes.arrayOf(PropTypes.shape({})),
  }).isRequired,
};

export default MeetupItem;
