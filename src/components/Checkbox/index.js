import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Container, Check, Label } from './styles';

class Checkbox extends Component {
  state = {
    isChecked: false,
  };

  componentDidMount() {
    const { isChecked } = this.props;
    if (isChecked) {
      this.setState({ isChecked });
    }
  }

  handleClick = () => {
    const { onChange, value } = this.props;
    const { isChecked } = this.state;
    this.setState({ isChecked: !isChecked }, onChange(!isChecked, value));
  };

  render() {
    const { label } = this.props;
    const { isChecked } = this.state;
    return (
      <Container onClick={this.handleClick} isChecked={isChecked}>
        <Check isChecked={isChecked} />
        <Label>{label}</Label>
      </Container>
    );
  }
}

Checkbox.defaultProps = {
  isChecked: false,
};

Checkbox.propTypes = {
  isChecked: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  label: PropTypes.string.isRequired,
};

export default Checkbox;
