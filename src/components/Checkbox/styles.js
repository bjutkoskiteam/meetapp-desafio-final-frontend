import styled from 'styled-components';

export const Check = styled.div`
  height: 20px;
  width: 20px;
  background-color: ${props => (props.isChecked ? '#e5556e' : '#555')};
  margin-right: 10px;
  border-radius: 5px;
  padding: 0 0 0 0 !important;
`;

export const Label = styled.span`
  color: #fff;
  font-size: 18px;
  font-weight: normal !important;
  padding: 0 0 0 0 !important;
  margin: 0 0 0 0 !important;
`;

export const Container = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;

  &:hover {
    ${Check} {
      background-color: ${props => (props.isChecked ? '#e5556e' : '#568')};
    }
  }

  &:active {
    ${Check} {
      background-color: #e5756e;
    }
  }
`;
