import styled from 'styled-components';

export const Container = styled.div``;

export const Title = styled.h1`
  font-weight: bold;
  font-size: 16px;
  padding: 15px 0;
`;

export const Meetups = styled.div`
  display: flex;
  flex-direction: row;
  overflow-x: auto;
  white-space: nowrap;
  flex-wrap: ${props => (props.wrap ? 'wrap' : 'nowrap')};

  div {
    margin-bottom: ${props => (props.wrap ? '20px' : '')};
  }
`;
