import React from 'react';
import PropTypes from 'prop-types';
import { Container, Title, Meetups } from './styles';
import MeetupItem from '../MeetupItem';

const MeetupList = ({ title, meetups, wrap }) => (
  <Container>
    <Title>{title}</Title>
    <Meetups wrap={wrap}>
      {meetups.map(meetup => (
        <MeetupItem key={meetup.id} meetup={meetup} />
      ))}
    </Meetups>
  </Container>
);

MeetupList.defaultProps = {
  wrap: false,
};

MeetupList.propTypes = {
  title: PropTypes.string.isRequired,
  meetups: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
  wrap: PropTypes.bool,
};

export default MeetupList;
