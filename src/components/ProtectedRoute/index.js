import React from 'react';

import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const PrivateRoute = ({
  component: Component, isAllowed, alternativePath, ...rest
}) => (
  <Route
    {...rest}
    render={props => (isAllowed ? <Component {...props} /> : <Redirect to={alternativePath} />)}
  />
);

PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  isAllowed: PropTypes.bool.isRequired,
  alternativePath: PropTypes.string.isRequired,
};

export default PrivateRoute;
