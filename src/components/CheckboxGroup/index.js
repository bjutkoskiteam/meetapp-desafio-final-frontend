import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Container } from './styles';
import Checkbox from '../Checkbox';

class CheckboxGroup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkedValues: props.checkedValues,
    };
  }

  componentDidMount() {
    const { checkedValues } = this.props;
    if (checkedValues && checkedValues.length > 0) {
      this.setState({ checkedValues });
    }
  }

  handleCheckboxClick = (isChecked, value) => {
    const { checkedValues } = this.state;
    const { onChange } = this.props;
    if (isChecked) {
      checkedValues.push(value);
    } else {
      const index = checkedValues.indexOf(value);

      if (index > -1) {
        checkedValues.splice(index, 1);
      }
    }
    this.setState({ checkedValues }, onChange(checkedValues));
  };

  render() {
    const { cbList } = this.props;
    const { checkedValues } = this.state;
    return (
      <Container>
        {cbList.map(cb => (
          <Checkbox
            label={cb.label}
            value={cb.value}
            onChange={this.handleCheckboxClick}
            isChecked={checkedValues.includes(cb.value)}
            key={cb.value}
          />
        ))}
      </Container>
    );
  }
}

CheckboxGroup.defaultProps = {
  checkedValues: [],
};

CheckboxGroup.propTypes = {
  checkedValues: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  cbList: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    }),
  ).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default CheckboxGroup;
