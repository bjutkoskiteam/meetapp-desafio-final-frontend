import styled from 'styled-components';

export const Container = styled.div`
  padding: 15px 0;
  div {
    padding: 5px 0;
  }
`;
