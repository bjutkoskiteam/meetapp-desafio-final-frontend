import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Menu } from './styles';

import Logo from '../../assets/images/logo-white.svg';
import ProfileImage from '../../assets/images/person_outline.png';

const Header = () => (
  <Container>
    <Menu>
      <img src={Logo} alt="Logo" />
      <Link to="/">Início</Link>
      <Link to="/search">Buscar</Link>
      <Link to="/newmeetup">Novo Meetup</Link>
    </Menu>
    <Link to="/profile">
      <img src={ProfileImage} alt="Profile" />
    </Link>
  </Container>
);

export default Header;
