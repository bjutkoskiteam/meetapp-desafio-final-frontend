import styled from 'styled-components';

export const Container = styled.header`
  width: 100%;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: #e5556e;
  color: #fff;
  overflow: hidden;
  position: fixed;
  top: 0;

  a {
    img {
      width: 24px;
      height: 24px;
      margin: 20px;

      &:hover {
        opacity: 0.4;
      }
    }
  }
`;

export const Menu = styled.div`
  display: flex;
  align-items: center;

  * {
    margin-left: 25px;
  }

  img {
    width: 20px;
    height: 20px;
  }

  a {
    font-size: 16px;
    font-weight: bold;
    color: inherit;
    text-decoration: none;

    &:hover {
      opacity: 0.4;
    }
  }
`;
