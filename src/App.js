import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import GlobalStyle from './styles/global';
import './config/reactotron';
import store, { history } from './store';
import Session from './components/Session';

import Routes from './routes';

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <GlobalStyle />
      <Session>
        <Routes />
      </Session>
    </ConnectedRouter>
  </Provider>
);

export default App;
