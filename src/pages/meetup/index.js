import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  Container, Content, Info, Title, OtherInfo,
} from './styles';
import Header from '../../components/Header';
import { Button } from '../../styles/components';

import { Creators as MeetupDetailsActions } from '../../store/ducks/meetupDetails';

class Meetup extends Component {
  componentDidMount() {
    this.loadMeetupDetails();
  }

  componentDidUpdate(prevProps) {
    const { match, meetupDetails } = this.props;
    if (
      prevProps.match.params.id !== match.params.id
      || prevProps.meetupDetails.subscribed !== meetupDetails.subscribed
    ) {
      this.loadMeetupDetails();
    }
  }

  loadMeetupDetails = () => {
    const { match, getMeetupDetailsRequest } = this.props;
    const { id } = match.params;

    getMeetupDetailsRequest(id);
  };

  handleSubscribe = (e) => {
    const {
      match, meetupDetails, subscribeRequest, unsubscribeRequest,
    } = this.props;
    const { id } = match.params;

    e.preventDefault();
    if (meetupDetails.subscribed) {
      unsubscribeRequest(id);
    } else {
      subscribeRequest(id);
    }
  };

  renderDetails = () => {
    const { meetupDetails } = this.props;
    return (
      <Container>
        <Header />
        <Content>
          <img src={meetupDetails.data.file.url} alt="Meetup" />
          <Info>
            <Title>
              <h1>{meetupDetails.data.title}</h1>
              <p>{`${meetupDetails.data.subscriptions.length} membros`}</p>
            </Title>
            <p>{meetupDetails.data.description}</p>
            <OtherInfo>
              <h1>Realizado em:</h1>
              <p>{meetupDetails.data.place}</p>
              <h1>Quando?</h1>
              <p>{meetupDetails.data.date}</p>
            </OtherInfo>
            <Button type="submit" onClick={this.handleSubscribe}>
              {meetupDetails.subscribed ? 'Desinscreva-se' : 'Inscreva-se'}
            </Button>
          </Info>
        </Content>
      </Container>
    );
  };

  render() {
    const { meetupDetails } = this.props;
    return meetupDetails.loading ? <h1>Carregando...</h1> : this.renderDetails();
  }
}

Meetup.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
  getMeetupDetailsRequest: PropTypes.func.isRequired,
  subscribeRequest: PropTypes.func.isRequired,
  unsubscribeRequest: PropTypes.func.isRequired,
  meetupDetails: PropTypes.shape({
    data: PropTypes.shape({
      file: PropTypes.shape({
        url: PropTypes.string.isRequired,
      }),
      title: PropTypes.string,
      description: PropTypes.string,
      place: PropTypes.string,
      date: PropTypes.string,
      subscriptions: PropTypes.arrayOf(PropTypes.shape({})),
    }),
    subscribed: PropTypes.bool,
  }).isRequired,
};

const mapStateToProps = state => ({
  meetupDetails: state.meetupDetails,
});

const mapDispatchToProps = dispatch => bindActionCreators(MeetupDetailsActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Meetup);
