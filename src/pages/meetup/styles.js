import styled from 'styled-components';
import { DefaultContainer } from '../../styles/components';

export const Container = styled(DefaultContainer)``;

export const Title = styled.div`
  h1 {
    font-size: 24px;
    font-weight: bold;
  }

  p {
    font-size: 14px;
    color: #999999;
    margin: 5px 0;
  }
`;

export const OtherInfo = styled.div`
  margin: 20px 0;

  h1 {
    font-weight: normal;
    font-size: 14px;
    color: #999999;
  }

  p {
    font-size: 14px;
    opacity: 0.8;
    line-height: 24px;
  }
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  width: 45%;

  p {
    font-size: 16px;
    opacity: 0.8;
    line-height: 28px;
    margin: 10px 0;
  }
`;

export const Content = styled.div`
  display: flex;
  align-items: stretch;
  flex-direction: column;
  width: 60%;
  margin-top: 40px;

  img {
    margin: 30px 0 20px 0;
    width: 100%;
    height: 60%;
  }
`;
