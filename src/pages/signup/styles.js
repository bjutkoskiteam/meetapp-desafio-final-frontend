import styled from 'styled-components';
import { DefaultContainer, SignForm } from '../../styles/components';

export const Container = styled(DefaultContainer)`
  a {
    font-size: 16px;
    color: #b3b3b3;
    text-decoration: none;

    &:hover {
      color: #fff;
    }
  }
`;

export const Login = styled(SignForm)`
  max-width: 280px;
`;
