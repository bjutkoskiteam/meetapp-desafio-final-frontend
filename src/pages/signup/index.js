import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Container, Login } from './styles';
import { Field, Button } from '../../styles/components';

import { Creators as SessionActions } from '../../store/ducks/session';

import Logo from '../../assets/images/logo.svg';

class SignUpPage extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    passwordConfirmation: '',
  };

  handleSubmit = (event) => {
    const { createUserRequest } = this.props;
    const {
      name, email, password, passwordConfirmation,
    } = this.state;

    event.preventDefault();
    createUserRequest({
      name,
      email,
      password,
      passwordConfirmation,
    });
  };

  render() {
    const {
      name, email, password, passwordConfirmation,
    } = this.state;
    return (
      <Container>
        <img src={Logo} alt="Meetap Logo" />
        <Login onSubmit={this.handleSubmit}>
          <Field>
            <span>Nome</span>
            <input
              type="text"
              placeholder="Digite seu nome"
              value={name}
              onChange={e => this.setState({ name: e.target.value })}
            />
          </Field>
          <Field>
            <span>Email</span>
            <input
              type="email"
              placeholder="Digite seu e-mail"
              value={email}
              onChange={e => this.setState({ email: e.target.value })}
            />
          </Field>
          <Field>
            <span>Senha</span>
            <input
              type="password"
              placeholder="Sua senha secreta"
              value={password}
              onChange={e => this.setState({ password: e.target.value })}
            />
          </Field>
          <Field>
            <span>Confirme sua senha</span>
            <input
              type="password"
              placeholder="Confirme sua senha secreta"
              value={passwordConfirmation}
              onChange={e => this.setState({ passwordConfirmation: e.target.value })}
            />
          </Field>
          <Button type="submit">Criar conta</Button>
        </Login>
        <Link to="/signin">Já tenho conta</Link>
      </Container>
    );
  }
}

SignUpPage.propTypes = {
  createUserRequest: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => bindActionCreators(SessionActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUpPage);
