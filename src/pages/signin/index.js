import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Container, Login } from './styles';
import { Field, Button } from '../../styles/components';

import { Creators as SessionActions } from '../../store/ducks/session';

import Logo from '../../assets/images/logo.svg';

class SignInPage extends Component {
  state = {
    email: '',
    password: '',
  };

  handleSubmit = (event) => {
    const { createSessionRequest, history } = this.props;
    const { email, password } = this.state;

    event.preventDefault();
    createSessionRequest(email, password);
    history.push('/');
  };

  render() {
    const { email, password } = this.state;
    return (
      <Container>
        <img src={Logo} alt="Meetap Logo" />
        <Login onSubmit={this.handleSubmit}>
          <Field>
            <span>Email</span>
            <input
              type="email"
              placeholder="Digite seu e-mail"
              value={email}
              onChange={e => this.setState({ email: e.target.value })}
            />
          </Field>
          <Field>
            <span>Senha</span>
            <input
              type="password"
              placeholder="Sua senha secreta"
              value={password}
              onChange={e => this.setState({ password: e.target.value })}
            />
          </Field>
          <Button type="submit">Entrar</Button>
        </Login>
        <Link to="/signup">Criar conta grátis</Link>
      </Container>
    );
  }
}

SignInPage.propTypes = {
  createSessionRequest: PropTypes.func.isRequired,
  history: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => bindActionCreators(SessionActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignInPage);
