import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Field, Button } from '../../styles/components';
import { Preferences, Container } from './styles';
import CheckboxGroup from '../../components/CheckboxGroup';
import { Creators as SessionActions } from '../../store/ducks/session';

class PreferencesPage extends Component {
  state = {
    preferences: [],
  };

  handleSubmit = (event) => {
    const { updateUserRequest } = this.props;
    event.preventDefault();
    updateUserRequest(this.state);
  };

  handleCheckedValues = (checkedValues) => {
    this.setState({ preferences: checkedValues });
  };

  render() {
    const { session } = this.props;
    const name = session.user.name ? session.user.name.split(' ')[0] : '';
    return (
      <Container>
        <Preferences onSubmit={this.handleSubmit}>
          <h1>
            Olá,
            {` ${name}`}
          </h1>
          <p>
            Parece que é seu primeiro acesso por aqui, comece escolhendo algumas preferências para
            selecionarmos os melhores meetups pra você:
          </p>
          <Field>
            <span>Preferências</span>
            <CheckboxGroup
              cbList={[
                { label: 'Front-end', value: 1 },
                { label: 'Back-end', value: 2 },
                { label: 'Mobile', value: 3 },
                { label: 'DevOps', value: 4 },
                { label: 'Gestao', value: 5 },
                { label: 'Marketing', value: 6 },
              ]}
              onChange={this.handleCheckedValues}
            />
          </Field>
          <Button type="submit">Continuar</Button>
        </Preferences>
      </Container>
    );
  }
}

PreferencesPage.propTypes = {
  updateUserRequest: PropTypes.func.isRequired,
  session: PropTypes.shape({
    user: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => bindActionCreators(SessionActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PreferencesPage);
