import styled from 'styled-components';
import { SignForm, DefaultContainer } from '../../styles/components';

export const Container = styled(DefaultContainer)`
  width: 100%;
  margin-top: 50px;
`;

export const Preferences = styled(SignForm)`
  max-width: 320px;

  h1 {
    font-size: 24px;
    font-weight: bold;
    margin-bottom: 20px;
  }

  p {
    font-size: 16px;
    line-height: 28px;
    opacity: 0.8;
  }

  span {
    margin-top: 30px;
  }
`;
