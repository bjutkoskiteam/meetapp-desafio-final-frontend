import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, StyledMeetups, SearchBox } from './styles';
import Header from '../../components/Header';

import MeetupList from '../../components/MeetupList';

import { Creators as MeetupsActions } from '../../store/ducks/meetups';

class Search extends Component {
  state = {
    searchString: '',
  };

  handleSubmit = (e) => {
    const { getSearchRequest } = this.props;
    const { searchString } = this.state;
    e.preventDefault();
    getSearchRequest(searchString);
    this.setState({ searchString: '' });
  };

  render() {
    const { meetups } = this.props;
    const { searchString } = this.state;
    return (
      <Container>
        <Header />
        <StyledMeetups>
          <SearchBox>
            <form onSubmit={this.handleSubmit}>
              <input
                placeholder="Buscar meetups"
                value={searchString}
                onChange={e => this.setState({ searchString: e.target.value })}
              />
            </form>
          </SearchBox>
          <MeetupList title="Resultados da busca" meetups={meetups.search} wrap />
        </StyledMeetups>
      </Container>
    );
  }
}

Search.propTypes = {
  getSearchRequest: PropTypes.func.isRequired,
  meetups: PropTypes.shape({
    coming: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
    subscribed: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
    recommended: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
  }).isRequired,
};

const mapStateToProps = state => ({
  meetups: state.meetups,
});

const mapDispatchToProps = dispatch => bindActionCreators(MeetupsActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);
