import styled from 'styled-components';

import SearchIcon from '../../assets/images/search.svg';

export const Container = styled.div`
  width: 100%;
  margin-top: 65px;
`;

export const StyledMeetups = styled.div`
  width: 75%;
  margin: 15px auto;
  display: flex;
  flex-direction: column;
  align-content: center;
`;

export const SearchBox = styled.div`
  display: flex;
  align-items: center;
  height: 36px;
  width: 100%;
  margin: 20px 0;
  padding: 6px 7px 6px 26px;
  background: #2f2d38 url(${SearchIcon}) no-repeat 7px center;

  form {
    width: 100%;
    height: 100%;
    input {
      flex: 1;
      font-size: 15px;
      color: #8e8e93;
      border: 0;
      background: #2f2d38;
      width: 100%;
      box-sizing: border-box;
    }
  }
`;
