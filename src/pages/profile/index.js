import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Header from '../../components/Header';

import { Container, ProfileForm } from './styles';
import { Field, Button } from '../../styles/components';

import CheckboxGroup from '../../components/CheckboxGroup';

import { Creators as SessionActions } from '../../store/ducks/session';

class Profile extends Component {
  constructor(props) {
    super(props);

    const { session } = props;

    this.state = {
      email: session.user.email,
      name: session.user.name,
      password: '',
      passwordConfirmation: '',
      preferences: session.user.preferences,
    };
  }

  handleSubmit = (event) => {
    const { updateUserRequest } = this.props;
    const newUser = { ...this.state };
    if (!newUser.password) {
      delete newUser.password;
      delete newUser.passwordConfirmation;
    }

    event.preventDefault();
    updateUserRequest(newUser);
  };

  handleCheckedValues = (checkedValues) => {
    this.setState({ preferences: checkedValues });
  };

  render() {
    const {
      name, password, passwordConfirmation, preferences,
    } = this.state;
    return (
      <Container>
        <Header />
        <ProfileForm onSubmit={this.handleSubmit}>
          <Field>
            <span>Nome</span>
            <input
              type="text"
              placeholder="Digite seu nome"
              value={name}
              onChange={e => this.setState({ name: e.target.value })}
            />
          </Field>
          <Field>
            <span>Senha</span>
            <input
              type="password"
              placeholder="Sua senha secreta"
              value={password}
              onChange={e => this.setState({ password: e.target.value })}
            />
          </Field>
          <Field>
            <span>Confirme sua senha</span>
            <input
              type="password"
              placeholder="Confirme sua senha secreta"
              value={passwordConfirmation}
              onChange={e => this.setState({ passwordConfirmation: e.target.value })}
            />
          </Field>
          <Field>
            <span>Preferências</span>
            <CheckboxGroup
              cbList={[
                { label: 'Front-end', value: 1 },
                { label: 'Back-end', value: 2 },
                { label: 'Mobile', value: 3 },
                { label: 'DevOps', value: 4 },
                { label: 'Gestao', value: 5 },
                { label: 'Marketing', value: 6 },
              ]}
              onChange={this.handleCheckedValues}
              checkedValues={preferences}
            />
          </Field>
          <Button type="submit">Salvar</Button>
        </ProfileForm>
      </Container>
    );
  }
}

Profile.propTypes = {
  updateUserRequest: PropTypes.func.isRequired,
  session: PropTypes.shape({
    user: PropTypes.shape({
      name: PropTypes.string,
      preferences: PropTypes.arrayOf(PropTypes.number),
    }),
  }).isRequired,
};

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => bindActionCreators(SessionActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
