import styled from 'styled-components';
import { DefaultContainer, SignForm } from '../../styles/components';

export const Container = styled(DefaultContainer)`
  width: 100%;
  margin-top: 80px;
`;

export const ProfileForm = styled(SignForm)`
  max-width: 280px;
  height: 100%;
`;
