import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  margin-top: 65px;
`;

export const StyledMeetups = styled.div`
  width: 75%;
  margin: 15px auto;
  display: flex;
  flex-direction: column;
  align-content: center;
`;
