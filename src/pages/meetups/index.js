import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, StyledMeetups } from './styles';
import Header from '../../components/Header';

import MeetupList from '../../components/MeetupList';

import { Creators as MeetupsActions } from '../../store/ducks/meetups';

class Meetups extends Component {
  componentDidMount() {
    const { getComingRequest, getSubscribedRequest, getRecommendedRequest } = this.props;
    getComingRequest();
    getSubscribedRequest();
    getRecommendedRequest();
  }

  render() {
    const { meetups } = this.props;
    return (
      <Container>
        <Header />
        <StyledMeetups>
          <MeetupList title="Inscrições" meetups={meetups.subscribed} />
          <MeetupList title="Próximos meetups" meetups={meetups.coming} />
          <MeetupList title="Recomendados" meetups={meetups.recommended} />
        </StyledMeetups>
      </Container>
    );
  }
}

Meetups.propTypes = {
  getComingRequest: PropTypes.func.isRequired,
  getSubscribedRequest: PropTypes.func.isRequired,
  getRecommendedRequest: PropTypes.func.isRequired,
  meetups: PropTypes.shape({
    coming: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
    subscribed: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
    recommended: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
  }).isRequired,
};

const mapStateToProps = state => ({
  meetups: state.meetups,
});

const mapDispatchToProps = dispatch => bindActionCreators(MeetupsActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Meetups);
