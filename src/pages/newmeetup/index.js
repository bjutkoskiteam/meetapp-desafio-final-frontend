import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Header from '../../components/Header';
import { Container, MeetupForm, ImageField } from './styles';
import CheckboxGroup from '../../components/CheckboxGroup';
import { Field, Button } from '../../styles/components';
import { Creators as MeetupActions } from '../../store/ducks/meetup';

class NewMeetup extends Component {
  state = {
    title: '',
    description: '',
    date: new Date(),
    place: '',
    preferences: [],
    file: null,
  };

  handleCheckedValues = (checkedValues) => {
    this.setState({ preferences: checkedValues });
  };

  handleImage = (e) => {
    const file = Array.from(e.target.files)[0];
    this.setState({ file });
  };

  handleSubmit = (e) => {
    const { createMeetupRequest } = this.props;
    e.preventDefault();
    createMeetupRequest(this.state);
  };

  render() {
    const {
      title, description, place, file, date,
    } = this.state;
    return (
      <Container>
        <Header />
        <MeetupForm onSubmit={this.handleSubmit}>
          <Field>
            <span>Título</span>
            <input
              type="text"
              placeholder="Digite o título do meetup"
              value={title}
              onChange={e => this.setState({ title: e.target.value })}
            />
          </Field>
          <Field>
            <span>Descrição</span>
            <input
              type="text"
              placeholder="Descreva seu meetup"
              value={description}
              onChange={e => this.setState({ description: e.target.value })}
            />
          </Field>
          <Field>
            <span>Data/hora</span>
            <input type="text" placeholder="Quando o meetup vai acontecer?" value={date} />
          </Field>
          <Field>
            <span>Imagem</span>
            <ImageField image={file && URL.createObjectURL(file)}>
              <input type="file" onChange={this.handleImage} />
            </ImageField>
          </Field>
          <Field>
            <span>Localização</span>
            <input
              type="text"
              placeholder="Onde seu meetup irá acontecer?"
              value={place}
              onChange={e => this.setState({ place: e.target.value })}
            />
          </Field>
          <Field>
            <span>Tema do meetup</span>
            <CheckboxGroup
              cbList={[
                { label: 'Front-end', value: 1 },
                { label: 'Back-end', value: 2 },
                { label: 'Mobile', value: 3 },
                { label: 'DevOps', value: 4 },
                { label: 'Gestao', value: 5 },
                { label: 'Marketing', value: 6 },
              ]}
              onChange={this.handleCheckedValues}
            />
          </Field>
          <Button type="submit">Salvar</Button>
        </MeetupForm>
      </Container>
    );
  }
}

NewMeetup.propTypes = {
  createMeetupRequest: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  meetup: state.meetup,
});

const mapDispatchToProps = dispatch => bindActionCreators(MeetupActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewMeetup);
