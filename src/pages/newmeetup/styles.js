import styled from 'styled-components';
import { DefaultContainer, SignForm } from '../../styles/components';
import CameraImage from '../../assets/images/camera_alt.svg';

export const Container = styled(DefaultContainer)`
  width: 100%;
  margin-top: 80px;
`;

export const MeetupForm = styled(SignForm)`
  max-width: 320px;
  height: 100%;
`;

export const ImageField = styled.div`
  display: flex;
  margin-top: 10px;
  border: 1px dashed #fff;
  opacity: ${props => (props.image ? 1.0 : 0.5)};

  input {
    cursor: pointer;
    width: 100%;
    height: 100px;
    margin: auto;
    background-color: #fff;
    -webkit-mask: ${props => (props.image ? null : `url(${CameraImage})`)};
    mask: ${props => (props.image ? null : `url(${CameraImage}) no-repeat center`)};
    background: ${props => (props.image ? `url(${props.image}) no-repeat` : null)};
    background-size: cover;
  }
`;
