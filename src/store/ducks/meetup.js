export const Types = {
  CREATE_REQUEST: 'meetup/CREATE_REQUEST',
  CREATE_SUCCESS: 'meetup/CREATE_SUCCESS',
};

const INITIAL_STATE = {
  data: {},
  loading: false,
};

export default function meetup(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.CREATE_REQUEST:
      return { ...state, loading: true };
    case Types.CREATE_SUCCESS:
      return { ...state, loading: false, data: action.payload.data };
    default:
      return state;
  }
}

export const Creators = {
  createMeetupRequest: newMeetup => ({
    type: Types.CREATE_REQUEST,
    payload: newMeetup,
  }),

  createMeetupSuccess: data => ({
    type: Types.CREATE_SUCCESS,
    payload: { data },
  }),
};
