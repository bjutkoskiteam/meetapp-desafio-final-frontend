export const Types = {
  GET_MEETUP_REQUEST: 'meetupDetails/GET_MEETUP_REQUEST',
  GET_MEETUP_SUCCESS: 'meetupDetails/GET_MEETUP_SUCCESS',
  GET_MEETUP_FAILURE: 'meetupDetails/GET_MEETUP_FAILURE',
  SUBSCRIBE_REQUEST: 'meetupDetails/SUBSCRIBE_REQUEST',
  SUBSCRIBE_SUCCESS: 'meetupDetails/SUBSCRIBE_SUCCESS',
  SUBSCRIBE_FAILURE: 'meetupDetails/SUBSCRIBE_FAILURE',
  UNSUBSCRIBE_REQUEST: 'meetupDetails/UNSUBSCRIBE_REQUEST',
  UNSUBSCRIBE_SUCCESS: 'meetupDetails/UNSUBSCRIBE_SUCCESS',
  UNSUBSCRIBE_FAILURE: 'meetupDetails/UNSUBSCRIBE_FAILURE',
};

const INITIAL_STATE = {
  data: {},
  subscribed: false,
  loading: true,
};

export default function meetupDetails(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_MEETUP_REQUEST:
    case Types.SUBSCRIBE_REQUEST:
    case Types.UNSUBSCRIBE_REQUEST:
      return { ...state, loading: true };
    case Types.GET_MEETUP_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data.meetup,
        subscribed: action.payload.data.isSubscribed,
      };
    case Types.GET_MEETUP_FAILURE:
    case Types.SUBSCRIBE_FAILURE:
    case Types.UNSUBSCRIBE_FAILURE:
      return { ...state, loading: false };
    case Types.SUBSCRIBE_SUCCESS:
      return { ...state, loading: false, subscribed: true };
    case Types.UNSUBSCRIBE_SUCCESS:
      return { ...state, loading: false, subscribed: false };
    default:
      return state;
  }
}

export const Creators = {
  getMeetupDetailsRequest: id => ({
    type: Types.GET_MEETUP_REQUEST,
    payload: { id },
  }),

  getMeetupDetailsSuccess: data => ({
    type: Types.GET_MEETUP_SUCCESS,
    payload: { data },
  }),

  getMeetupDetailsFailure: () => ({
    type: Types.GET_MEETUP_FAILURE,
  }),

  subscribeRequest: id => ({
    type: Types.SUBSCRIBE_REQUEST,
    payload: { id },
  }),

  subscribeSuccess: () => ({
    type: Types.SUBSCRIBE_SUCCESS,
  }),

  subscribeFailure: () => ({
    type: Types.SUBSCRIBE_FAILURE,
  }),

  unsubscribeRequest: id => ({
    type: Types.UNSUBSCRIBE_REQUEST,
    payload: { id },
  }),

  unsubscribeSuccess: () => ({
    type: Types.UNSUBSCRIBE_SUCCESS,
  }),

  unsubscribeFailure: () => ({
    type: Types.UNSUBSCRIBE_FAILURE,
  }),
};
