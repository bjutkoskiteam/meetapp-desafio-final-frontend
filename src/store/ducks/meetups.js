export const Types = {
  GET_SUBSCRIBED_REQUEST: 'meetups/GET_SUBSCRIBED_REQUEST',
  GET_SUBSCRIBED_SUCCESS: 'meetups/GET_SUBSCRIBED_SUCCESS',
  GET_SUBSCRIBED_FAILURE: 'meetups/GET_SUBSCRIBED_FAILURE',
  GET_COMING_REQUEST: 'meetups/GET_COMING_REQUEST',
  GET_COMING_SUCCESS: 'meetups/GET_COMING_SUCCESS',
  GET_COMING_FAILURE: 'meetups/GET_COMING_FAILURE',
  GET_RECOMMENDED_REQUEST: 'meetups/GET_RECOMMENDED_REQUEST',
  GET_RECOMMENDED_SUCCESS: 'meetups/GET_RECOMMENDED_SUCCESS',
  GET_RECOMMENDED_FAILURE: 'meetups/GET_RECOMMENDED_FAILURE',
  GET_SEARCH_REQUEST: 'meetups/GET_SEARCH_REQUEST',
  GET_SEARCH_SUCCESS: 'meetups/GET_SEARCH_SUCCESS',
  GET_SEARCH_FAILURE: 'meetups/GET_SEARCH_FAILURE',
};

const INITIAL_STATE = {
  subscribed: [],
  coming: [],
  recommended: [],
  search: [],
  loading: false,
};

export default function meetups(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_COMING_REQUEST:
    case Types.GET_SUBSCRIBED_REQUEST:
    case Types.GET_RECOMMENDED_REQUEST:
    case Types.GET_SEARCH_REQUEST:
      return { ...state, loading: true };
    case Types.GET_COMING_SUCCESS:
      return { ...state, loading: false, coming: action.payload.data };
    case Types.GET_SUBSCRIBED_SUCCESS:
      return { ...state, loading: false, subscribed: action.payload.data };
    case Types.GET_RECOMMENDED_SUCCESS:
      return { ...state, loading: false, recommended: action.payload.data };
    case Types.GET_SEARCH_SUCCESS:
      return { ...state, loading: false, search: action.payload.data };
    case Types.GET_COMING_FAILURE:
    case Types.GET_SUBSCRIBED_FAILURE:
    case Types.GET_RECOMMENDED_FAILURE:
    case Types.GET_SEARCH_FAILURE:
      return { ...state, loading: false };
    default:
      return state;
  }
}

export const Creators = {
  getComingRequest: () => ({
    type: Types.GET_COMING_REQUEST,
  }),

  getComingSuccess: data => ({
    type: Types.GET_COMING_SUCCESS,
    payload: { data },
  }),

  getComingFailure: () => ({
    type: Types.GET_COMING_FAILURE,
  }),

  getSubscribedRequest: () => ({
    type: Types.GET_SUBSCRIBED_REQUEST,
  }),

  getSubscribedSuccess: data => ({
    type: Types.GET_SUBSCRIBED_SUCCESS,
    payload: { data },
  }),

  getSubscribedFailure: () => ({
    type: Types.GET_SUBSCRIBED_FAILURE,
  }),

  getRecommendedRequest: () => ({
    type: Types.GET_RECOMMENDED_REQUEST,
  }),

  getRecommendedSuccess: data => ({
    type: Types.GET_RECOMMENDED_SUCCESS,
    payload: { data },
  }),

  getRecommendedFailure: () => ({
    type: Types.GET_RECOMMENDED_FAILURE,
  }),

  getSearchRequest: searchString => ({
    type: Types.GET_SEARCH_REQUEST,
    payload: { searchString },
  }),

  getSearchSuccess: data => ({
    type: Types.GET_SEARCH_SUCCESS,
    payload: { data },
  }),

  getSearchFailure: () => ({
    type: Types.GET_SEARCH_FAILURE,
  }),
};
