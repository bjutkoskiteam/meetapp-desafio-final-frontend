import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import session from './session';
import meetup from './meetup';
import meetups from './meetups';
import meetupDetails from './meetupDetails';

export default history => combineReducers({
  router: connectRouter(history),
  session,
  meetup,
  meetups,
  meetupDetails,
});
