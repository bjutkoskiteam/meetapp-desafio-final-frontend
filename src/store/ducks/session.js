export const Types = {
  VERIFY_REQUEST: 'session/VERIFY_REQUEST',
  VERIFY_SUCCESS: 'session/VERIFY_SUCCESS',
  VERIFY_FAILURE: 'session/VERIFY_FAILURE',
  CREATE_REQUEST: 'session/CREATE_REQUEST',
  CREATE_SUCCESS: 'session/CREATE_SUCCESS',
  CREATE_FAILURE: 'session/CREATE_FAILURE',
  CREATE_USER_REQUEST: 'session/CREATE_USER_REQUEST',
  CREATE_USER_SUCCESS: 'session/CREATE_USER_SUCCESS',
  CREATE_USER_FAILURE: 'session/CREATE_USER_FAILURE',
  UPDATE_USER_REQUEST: 'session/UPDATE_USER_REQUEST',
  UPDATE_USER_SUCCESS: 'session/UPDATE_USER_SUCCESS',
  UPDATE_USER_FAILURE: 'session/UPDATE_USER_FAILURE',
};

const INITIAL_STATE = {
  user: {},
  token: '',
  loading: false,
  isAuthenticated: false,
};

export default function session(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.VERIFY_REQUEST:
    case Types.CREATE_REQUEST:
      return {
        ...state,
        loading: true,
        isAuthenticated: true,
      };
    case Types.VERIFY_SUCCESS:
    case Types.CREATE_SUCCESS:
    case Types.CREATE_USER_SUCCESS:
    case Types.UPDATE_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload.user,
        token: action.payload.token,
        isAuthenticated: true,
      };
    case Types.VERIFY_FAILURE:
    case Types.CREATE_FAILURE:
    case Types.CREATE_USER_FAILURE:
    case Types.UPDATE_USER_FAILURE:
      return {
        ...state,
        loading: false,
        user: {},
        token: '',
        isAuthenticated: false,
      };
    case Types.CREATE_USER_REQUEST:
    case Types.UPDATE_USER_REQUEST:
      return { ...state, loading: true, isAuthenticated: true };
    default:
      return state;
  }
}

export const Creators = {
  verifySessionRequest: () => ({
    type: Types.VERIFY_REQUEST,
  }),

  verifySessionSuccess: (user, token) => ({
    type: Types.VERIFY_SUCCESS,
    payload: { user, token },
  }),

  verifySessionFailure: () => ({
    type: Types.VERIFY_FAILURE,
  }),

  createSessionRequest: (email, password) => ({
    type: Types.CREATE_REQUEST,
    payload: { email, password },
  }),

  createSessionSuccess: (user, token) => ({
    type: Types.CREATE_SUCCESS,
    payload: { user, token },
  }),

  createSessionFailure: () => ({
    type: Types.CREATE_FAILURE,
  }),

  createUserRequest: newUser => ({
    type: Types.CREATE_USER_REQUEST,
    payload: newUser,
  }),

  createUserSuccess: (user, token) => ({
    type: Types.CREATE_USER_SUCCESS,
    payload: { user, token },
  }),

  createUserFailure: () => ({
    type: Types.CREATE_USER_FAILURE,
  }),

  updateUserRequest: newUser => ({
    type: Types.UPDATE_USER_REQUEST,
    payload: newUser,
  }),

  updateUserSuccess: (user, token) => ({
    type: Types.UPDATE_USER_SUCCESS,
    payload: { user, token },
  }),

  updateUserFailure: () => ({
    type: Types.UPDATE_USER_FAILURE,
  }),
};
