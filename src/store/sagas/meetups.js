import { call, put, select } from 'redux-saga/effects';

import api from '../../services/api';

import { Creators as MeetupsActions } from '../ducks/meetups';
import { getToken } from '../selectors';

export function* getComingMeetups() {
  try {
    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const comingMeetups = yield call(api.get, '/meetups/coming', config);
    yield put(MeetupsActions.getComingSuccess(comingMeetups.data));
  } catch (err) {
    yield put(MeetupsActions.getComingFailure());
  }
}

export function* getSubscribedMeetups() {
  try {
    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const subscribedMeetups = yield call(api.get, '/meetups/subscribed', config);
    yield put(MeetupsActions.getSubscribedSuccess(subscribedMeetups.data));
  } catch (err) {
    yield put(MeetupsActions.getSubscribedFailure());
  }
}

export function* getRecommendedMeetups() {
  try {
    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const recommendedMeetups = yield call(api.get, '/meetups/recommended', config);
    yield put(MeetupsActions.getRecommendedSuccess(recommendedMeetups.data));
  } catch (err) {
    yield put(MeetupsActions.getRecommendedFailure());
  }
}

export function* getSearchMeetups(action) {
  try {
    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const searchMeetups = yield call(
      api.get,
      `/meetups?title=${action.payload.searchString}`,
      config,
    );
    yield put(MeetupsActions.getSearchSuccess(searchMeetups.data));
  } catch (err) {
    yield put(MeetupsActions.getSearchFailure());
  }
}
