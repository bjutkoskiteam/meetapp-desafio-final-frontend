import { call, put, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { getToken } from '../selectors';

import api from '../../services/api';

import { Creators as SessionActions } from '../ducks/session';

export function* verifySession() {
  try {
    const token = localStorage.getItem('@Meetapp:token');

    if (token) {
      const config = {
        headers: { Authorization: `bearer ${token}` },
      };

      const response = yield call(api.get, '/protected', config);
      yield put(SessionActions.verifySessionSuccess(response.data, token));
    } else {
      localStorage.removeItem('@Meetapp:token');
      yield put(SessionActions.verifySessionFailure());
    }
  } catch (err) {
    call(localStorage.removeItem, '@Meetapp:token');
    yield put(SessionActions.verifySessionFailure());
  }
}

export function* createSession(action) {
  try {
    const sessionResponse = yield call(api.post, '/sessions', action.payload);
    const { token } = sessionResponse.data;

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const protectedResponse = yield call(api.get, '/protected', config);
    localStorage.setItem('@Meetapp:token', token);
    yield put(SessionActions.createSessionSuccess(protectedResponse.data, token));
  } catch (err) {
    call(localStorage.removeItem, '@Meetapp:token');
    yield put(SessionActions.createSessionFailure());
  }
}

export function* createUser(action) {
  try {
    yield call(api.post, '/users', action.payload);
    const sessionResponse = yield call(api.post, '/sessions', action.payload);
    const { token } = sessionResponse.data;

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const protectedResponse = yield call(api.get, '/protected', config);
    localStorage.setItem('@Meetapp:token', token);
    yield put(push('/preferences'));
    yield put(SessionActions.createUserSuccess(protectedResponse.data, token));
  } catch (err) {
    call(localStorage.removeItem, '@Meetapp:token');
    yield put(SessionActions.createUserFailure());
  }
}

export function* updateUser(action) {
  try {
    let token = yield select(getToken);

    let config = {
      headers: { Authorization: `bearer ${token}` },
    };
    const response = yield call(api.put, '/users', action.payload, config);
    let updatedUser = response.data;
    if (action.payload.password) {
      const sessionResponse = yield call(api.post, '/sessions', action.payload);
      const { token: newToken } = sessionResponse.data;
      token = newToken;

      config = {
        headers: { Authorization: `bearer ${token}` },
      };

      const protectedResponse = yield call(api.get, '/protected', config);
      updatedUser = protectedResponse.data;
      localStorage.setItem('@Meetapp:token', token);
    }
    yield put(push('/'));
    yield put(SessionActions.updateUserSuccess(updatedUser, token));
  } catch (err) {
    call(localStorage.removeItem, '@Meetapp:token');
    yield put(SessionActions.updateUserFailure());
  }
}
