import { all, takeLatest } from 'redux-saga/effects';

import { Types as SessionTypes } from '../ducks/session';
import { Types as MeetupTypes } from '../ducks/meetup';
import { Types as MeetupDetailsTypes } from '../ducks/meetupDetails';
import { Types as MeetupsTypes } from '../ducks/meetups';

import {
  verifySession, createSession, createUser, updateUser,
} from './session';
import { createMeetup } from './meetup';
import {
  getComingMeetups,
  getSubscribedMeetups,
  getRecommendedMeetups,
  getSearchMeetups,
} from './meetups';
import { getMeetupDetails, subscribe, unsubscribe } from './meetupDetails';

export default function* rootSaga() {
  yield all([
    takeLatest(SessionTypes.VERIFY_REQUEST, verifySession),
    takeLatest(SessionTypes.CREATE_REQUEST, createSession),
    takeLatest(SessionTypes.CREATE_USER_REQUEST, createUser),
    takeLatest(SessionTypes.UPDATE_USER_REQUEST, updateUser),
    takeLatest(MeetupTypes.CREATE_REQUEST, createMeetup),
    takeLatest(MeetupsTypes.GET_COMING_REQUEST, getComingMeetups),
    takeLatest(MeetupsTypes.GET_SUBSCRIBED_REQUEST, getSubscribedMeetups),
    takeLatest(MeetupsTypes.GET_RECOMMENDED_REQUEST, getRecommendedMeetups),
    takeLatest(MeetupsTypes.GET_SEARCH_REQUEST, getSearchMeetups),
    takeLatest(MeetupDetailsTypes.GET_MEETUP_REQUEST, getMeetupDetails),
    takeLatest(MeetupDetailsTypes.SUBSCRIBE_REQUEST, subscribe),
    takeLatest(MeetupDetailsTypes.UNSUBSCRIBE_REQUEST, unsubscribe),
  ]);
}
