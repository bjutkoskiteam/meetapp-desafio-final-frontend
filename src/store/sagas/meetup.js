import { call, put, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import api from '../../services/api';

import { Creators as MeetupActions } from '../ducks/meetup';
import { getToken } from '../selectors';

export function* createMeetup(action) {
  try {
    const {
      title, description, place, date, preferences,
    } = action.payload;

    const newMeetup = {
      title,
      description,
      place,
      date,
      preferences,
    };

    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const { file } = action.payload;
    if (file) {
      const formData = new FormData();
      formData.append('file', file, file.name);
      const responseFile = yield call(api.post, '/files', formData, config);
      newMeetup.file_id = responseFile.data.id;
    }

    const responseMeetup = yield call(api.post, '/meetups', newMeetup, config);
    yield put(push('/'));
    yield put(MeetupActions.createMeetupSuccess(responseMeetup.data));
  } catch (err) {
    yield put(MeetupActions.createMeetupFailure());
  }
}
