import { call, put, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import api from '../../services/api';

import { Creators as MeetupDetailsActions } from '../ducks/meetupDetails';
import { getToken } from '../selectors';

export function* getMeetupDetails(action) {
  try {
    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    const response = yield call(api.get, `/meetups/${action.payload.id}`, config);
    yield put(MeetupDetailsActions.getMeetupDetailsSuccess(response.data));
  } catch (err) {
    yield put(MeetupDetailsActions.getMeetupDetailsFailure());
  }
}

export function* subscribe(action) {
  try {
    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    yield call(api.post, `/meetups/${action.payload.id}/subscribe`, null, config);
    yield put(push('/'));
    yield put(MeetupDetailsActions.subscribeSuccess());
  } catch (err) {
    yield put(MeetupDetailsActions.subscribeFailure());
  }
}

export function* unsubscribe(action) {
  try {
    const token = yield select(getToken);

    const config = {
      headers: { Authorization: `bearer ${token}` },
    };

    yield call(api.delete, `/meetups/${action.payload.id}/subscribe`, config);
    yield put(push('/'));
    yield put(MeetupDetailsActions.unsubscribeSuccess());
  } catch (err) {
    yield put(MeetupDetailsActions.unsubscribeFailure());
  }
}
