import React from 'react';

import { Switch } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Creators as SessionActions } from '../store/ducks/session';
import ProtectedRoute from '../components/ProtectedRoute';

import SignIn from '../pages/signin';
import SignUp from '../pages/signup';
import Preferences from '../pages/preferences';
import Meetups from '../pages/meetups';
import Meetup from '../pages/meetup';
import Search from '../pages/search';
import Profile from '../pages/profile';
import NewMeetup from '../pages/newmeetup';

const Routes = ({ session }) => (
  <Switch>
    <ProtectedRoute
      path="/signup"
      component={SignUp}
      isAllowed={!session.isAuthenticated}
      alternativePath="/"
    />
    <ProtectedRoute
      path="/signin"
      component={SignIn}
      isAllowed={!session.isAuthenticated}
      alternativePath="/"
    />
    <ProtectedRoute
      path="/preferences"
      component={Preferences}
      isAllowed={session.isAuthenticated}
      alternativePath="/signin"
    />
    <ProtectedRoute
      path="/search"
      component={Search}
      isAllowed={session.isAuthenticated}
      alternativePath="/signin"
    />
    <ProtectedRoute
      path="/profile"
      component={Profile}
      isAllowed={session.isAuthenticated}
      alternativePath="/signin"
    />
    <ProtectedRoute
      path="/newmeetup"
      component={NewMeetup}
      isAllowed={session.isAuthenticated}
      alternativePath="/signin"
    />
    <ProtectedRoute
      path="/meetups/:id"
      component={Meetup}
      isAllowed={session.isAuthenticated}
      alternativePath="/signin"
    />
    <ProtectedRoute
      path="/"
      component={Meetups}
      isAllowed={session.isAuthenticated}
      alternativePath="/signin"
    />
  </Switch>
);

Routes.propTypes = {
  session: PropTypes.shape({
    isAuthenticated: PropTypes.bool.isRequired,
  }).isRequired,
};

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => bindActionCreators(SessionActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Routes);
